﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fenglisu.Base.Contexts;
using Fenglisu.Base.Models;
using Fenglisu.Bff.ServiceContracts;
using Fenglisu.Bff.ServiceContracts.Model;
using Microsoft.AspNetCore.Authorization;

namespace Fenglisu.Bff.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly IMenuService _menuService;

        public MenuController(IMenuService menuService)
        {
            _menuService = menuService;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<Result<List<MenuItemModel>>> GetMenus()
        {
            var res = await _menuService.GetMenus();
            return res;
        }
    }
}
