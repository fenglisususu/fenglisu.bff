﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fenglisu.Base.Models;
using Fenglisu.Bff.ServiceContracts;
using Fenglisu.Bff.ServiceContracts.Model;
using Microsoft.AspNetCore.Authorization;

namespace Fenglisu.Bff.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IHomeService _homeService;

        public HomeController(IHomeService homeService)
        {
            _homeService = homeService;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<Result<List<string>>> TestApi(Person input)
        {
            return await _homeService.TestApi(input);
        }

        [HttpPost]
        public async Task<Result<List<string>>> TestApi2()
        {
            return await Task.FromResult(new Result<List<string>>());
        }
    }
}
