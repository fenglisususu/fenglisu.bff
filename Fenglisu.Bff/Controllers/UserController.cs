﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fenglisu.Base.Contexts;
using Fenglisu.Base.Models;
using Fenglisu.Bff.ServiceContracts;
using Fenglisu.Bff.ServiceContracts.Model;
using Microsoft.AspNetCore.Authorization;

namespace Fenglisu.Bff.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly UserContextManager _contextManager;

        public UserController(IUserService userService, UserContextManager contextManager)
        {
            _userService = userService;
            _contextManager = contextManager;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<Result<LoginResponse>> Login(LoginRequest request)
        {
            var result = await _userService.Login(request);
            if (result != null)
            {
                var user = result.Model;
                _contextManager.SetUserContext(user.Id, user.Name, user.Account);
                return Result.OutputSuccess(new LoginResponse()
                {
                    Account = user.Account,
                    Name = user.Name,
                    Id = user.Id,
                    Token = UserContext.Current.Token
                });
            }

            return Result.OutputFail<LoginResponse>(result.Msg);
        }

        [HttpPost]
        public async Task<Result<List<string>>> GetUserPics(GetUserPicsRequest request)
        {
            return await _userService.GetUserPics(request);
        }

        [HttpPost]
        public async Task<Result<bool>> SaveUserPics(SaveUserPicsRequest request)
        {
            return await _userService.SaveUserPics(request);
        }

        [HttpPost]
        public async Task<Result<bool>> RemoveUserPic(RemoveUserPicRequest request)
        {
            return await _userService.RemoveUserPic(request);
        }
    }
}
