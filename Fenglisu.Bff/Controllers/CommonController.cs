﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Fenglisu.Base.Contexts;
using Fenglisu.Base.Cryptography;
using Fenglisu.Base.Exceptions;
using Fenglisu.Base.Models;
using Fenglisu.Bff.ServiceContracts;
using Fenglisu.Bff.ServiceContracts.Model;

namespace Fenglisu.Bff.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        private readonly ICommonService _commonService;
        public CommonController(ICommonService commonService)
        {
            _commonService = commonService;
        }

        [HttpPost]
        public async Task<Result<string>> Upload()
        {
            var files = Request.Form.Files;
            if (files == null || files.Count == 0) throw new FenglisuException("请上传文件！");
            var file = files[0];
            byte[] bytes;
            using (var reader = new BinaryReader(file.OpenReadStream()))
            {
                bytes = reader.ReadBytes((int)file.Length);
            }
            string key = $"{UserContext.Current.Id}{HashEncryptHelper.MD5Encrypt(file.FileName)}{Path.GetExtension(file.FileName)}";
            var res = await _commonService.Upload(new UploadRequset() { Data = bytes, Key = key, FileName = file.FileName });
            return res;
        }
    }
}
