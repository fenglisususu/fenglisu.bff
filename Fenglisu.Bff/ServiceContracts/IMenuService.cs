﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fenglisu.Base.Models;
using Fenglisu.Base.Services;
using Fenglisu.Bff.ServiceContracts.Model;
using Refit;

namespace Fenglisu.Bff.ServiceContracts
{
    [ServiceName("fenglisu")]
    public interface IMenuService : IService
    {
        [Post("/api/menu/getmenus")]
        Task<Result<List<MenuItemModel>>> GetMenus();
    }
}
