﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fenglisu.Base.Models;
using Fenglisu.Base.Services;
using Fenglisu.Bff.ServiceContracts.Model;
using Refit;

namespace Fenglisu.Bff.ServiceContracts
{
    [ServiceName("fenglisu")]
    public interface ICommonService : IService
    {
        [Post("/api/common/upload")]
        Task<Result<string>> Upload(UploadRequset req);
    }
}
