﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fenglisu.Base.Models;
using Fenglisu.Base.Services;
using Fenglisu.Bff.ServiceContracts.Model;
using Refit;

namespace Fenglisu.Bff.ServiceContracts
{
    [ServiceName("fenglisu")]
    public interface IUserService : IService
    {
        [Post("/api/user/login")]
        Task<Result<LoginResponse>> Login(LoginRequest request);

        [Post("/api/user/getuserpics")]
        Task<Result<List<string>>> GetUserPics(GetUserPicsRequest request);

        [Post("/api/user/saveuserpics")]
        Task<Result<bool>> SaveUserPics(SaveUserPicsRequest request);

        [Post("/api/user/removeuserpic")]
        Task<Result<bool>> RemoveUserPic(RemoveUserPicRequest request);
    }
}
