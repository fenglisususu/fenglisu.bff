﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fenglisu.Bff.ServiceContracts.Model
{
    public class UploadRequset
    {
        public string FileName { get; set; }
        public byte[] Data { get; set; }
        public string Key { get; set; }
    }
}
