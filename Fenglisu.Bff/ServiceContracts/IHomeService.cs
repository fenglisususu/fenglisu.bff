﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fenglisu.Base.Models;
using Fenglisu.Base.Services;
using Fenglisu.Bff.ServiceContracts.Model;
using Refit;

namespace Fenglisu.Bff.ServiceContracts
{
    [ServiceName("fenglisu")]
    public interface IHomeService : IService
    {
        [Post("/api/home/testapi")]
        Task<Result<List<string>>> TestApi(Person input);

        [Post("/api/home/testapi2")]
        Task<Result<List<string>>> TestApi2();
    }
}
