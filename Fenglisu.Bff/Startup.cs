using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fenglisu.Base.Services;
using Fenglisu.Bff.ServiceContracts;
using Fenglisu.Consul;
using Fenglisu.Http;
using Fenglisu.Redis;
using Fenglisu.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Authorization;

namespace Fenglisu.Bff
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Env;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication("Token").AddToken("Token");

            services.AddControllers(options =>
            {
                var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Fenglisu.Bff", Version = "v1" });
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddCors();

            // 添加Redis
            services.AddRedisCache(Configuration.GetSection("Redis"));

            // 添加Consul
            services.AddConsul(Configuration.GetSection("Consul"))
                // Consul服务注册与发现
                .AddConsulServiceRegistry(Configuration.GetSection("ServiceRegistry"));

            var loadBalancer = Env.IsDevelopment() ? LoadBalancers.LocalFirst : LoadBalancers.RoundRobin;
            services.AddHttpService(loadBalancer)
                .AddService<IHomeService>()
                .AddService<IUserService>()
                .AddService<IMenuService>()
                .AddService<ICommonService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //健康监测中间件
            //app.UseHealthCheck();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Fenglisu.Bff v1"));
            }

            app.UseCors(builder => builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(build => { build.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod(); });

            // 使用token身份验证
            app.UseTokenAuthentication();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
